var link_to = function (url) {
    debug('<li>Click: <a href="' + url + '" target="_blank">' + url + '</a></li>');
};

// Get a RSS feed of the latest translations.
link_to('https://l10n.fs.al/btr/rss-feed');
link_to('https://l10n.fs.al/btr/rss-feed/vocabulary');
link_to('https://l10n.fs.al/btr/rss-feed/vocabulary/ICT_sq');

link_to('https://btranslator.fs.al/btr/rss-feed/LibreOffice/cui/sq');
link_to('https://btranslator.fs.al/btr/rss-feed/LibreOffice/cui');

link_to('https://btr.fs.al/rss-feed/sq');
link_to('https://btr.fs.al/rss-feed/sq/LibreOffice');
link_to('https://btr.fs.al/rss-feed/sq/LibreOffice/cui');
