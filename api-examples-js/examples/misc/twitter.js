var link_to = function (url) {
    debug('<li>Click: <a href="' + url + '" target="_blank">' + url + "</a></li>");
};

// Get a random tweet.
link_to('https://l10n.fs.al/btr/tweet');
link_to('https://l10n.fs.al/btr/tweet/vocabulary/ICT_sq');

link_to('https://btranslator.fs.al/btr/tweet/vocabulary/ICT_sq/sq');

link_to('https://btr.fs.al/tweet/sq');
link_to('https://btr.fs.al/tweet/sq/vocabulary/ICT_sq');

http_request('https://btr.fs.al/tweet/sq',
             { headers: { 'Accept': 'application/json'}});
