// Autocomplete strings.
var url = 'https://btr.fs.al/auto/string/vocabulary/ICT_sq/a';
http_request(url).done(function () {
    // Autocomplete strings with project and origin wildcards.
    var url = 'https://btr.fs.al/auto/string/*/*/b';
    http_request(url).done(function () {
        // Autocomplete projects.
        var url = 'https://btr.fs.al/auto/project/kd';
        http_request(url).done(function () {
            // Autocomplete origins of projects.
            var url = 'https://btr.fs.al/auto/origin/G';
            http_request(url).done(function () {
                // Autocomplete users.
                var url = 'https://btr.fs.al/auto/user/sq/d';
                http_request(url);
            });
        });
    });
});

